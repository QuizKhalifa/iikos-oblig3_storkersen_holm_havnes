
# For anyone reading this, instead of \n or \r, you use `n or `r (In essence, replace \ with `)

while($true) {

	Clear-Host; # Clear the screen
	"`n`t1 - Hvem er jeg og hva er navnet på dette scriptet?"
	"`t2 - Hvor lenge er det siden siste boot?"
	"`t3 - Hvor mange prosesser og tråder finnes?"
	"`t4 - Hvor mange context switch'er fant sted siste sekund?"
	"`t5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?"
	"`t6 - Hvor mange interrupts fant sted siste sekund?"
	"`t9 - Avslutt dette scriptet"

	""
	$choice = Read-Host -Prompt " Velg en funksjon"

	Clear-Host 			# Clears the screen (cls is an alias, but aliases shouldn't be used in scripts)
	switch ($choice)
	{
		1 { # Dette skriver ut navnet til brukeren og scriptet.
			$var = $MyInvocation.MyCommand.Name
			$user = $env:UserName
			"Jeg er $user, og dette scriptet heter $var."; 
			break}
		2 { # Skriver ut opptiden i dager, timer, minutter og sekunder til skjermen med forklaring hva som er hva.
			$var=$((get-date) - (gcim Win32_OperatingSystem).LastBootUpTime | select Days,Hours,Minutes,Seconds)
			"$($var)"; # It no work when printing table as is :(
			break}
		3 { # Skriver ut antall prosesser og tråder
			"Antall prosesser: $((Get-Process *).Count)"
			"Antall Threads: $(((gcim Win32_Thread).Handle).Count)";
			break}
		4 { # Antall kontekst-switcher i det siste sekundet.
			"Context switches: $(((((get-counter -Counter "\System\Context Switches/sec").CounterSamples).CookedValue).ToString()).split('.')[0])";
			# Bruk ved norsk OS: "Konteksvekslinger: $(((((get-counter -Counter "\System\Kontekstvekslinger/sek").CounterSamples).CookedValue).ToString()).split('.')[0])";
			break}
		5 { # Regner ut hvor mange milisekunder prosessoren var i kernel og user mode i det siste sekundet.
			$privpr=$(((get-counter -Counter "\Processor(*)\% Privileged Time").countersamples | Measure-Object -Property cookedvalue -sum).sum)
			$usrpr=$(((get-counter -Counter "\Processor(*)\% User Time").countersamples | Measure-Object -Property cookedvalue -sum).sum)
			$privtim=$(1000 * ($privpr / 100))
			$usrtim=$(1000 * ($usrpr / 100))
			"Time in user mode (ms): $($usrtim)";
			"Time in kernel/privileged mode (ms): $($privtim)";
			"Rest is idle."
			break}
		6 { # Antall interrupts det siste sekundet
			$inter = (((get-counter -Counter "\Processor(_total)\Interrupts/sec").countersamples).cookedvalue.ToString()).split('.')[0]
			"Antall interrupts det siste sekundet (totalt): $($inter)";
			break}
		9 {exit}
	}
	# Wait for input from user before returning to menu
	Write-Host -NoNewline "`nPress any key to continue...";
	$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
}
