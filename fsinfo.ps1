
if ($args.Count -eq 1)
{	
	# Get drive data
	$item = Get-Item $args[0]                        # Get-Item gets the item pointed to
	$drive = $item.PSdrive                           # .PSDrive gets info about the drive it is on
	$driveLetter = $drive.Name 			 # Get the drive name
	$driveUsage = ($drive.Used / $drive.Free) * 100  # MATH

	# Get file data
	$fileTable = Get-Childitem $($item.FullName) -Recurse -File # Get-ChildItem gets all items below the pointed-to file
							         # -Recurse recursively goes through all folders
							         # -File only get files (Not directories)
	$fileCount = $fileTable.Count    # .Count counts all entries in the resulting table
	
	# get largest file
	$fileLarge = ($fileTable | sort Length -Descending)[0] # Sort the table so that the top is the largest file and get it
	$fileLargeName = $fileLarge.FullName	               # Get its name
	$fileLargeSize = $fileLarge.Length		       # I'm not implementing human readable support

	# Get the average
	$fileTotal = $fileTable | Measure-Object -Property Length -sum # Measure-Object allows you to measure the object (duh)
								       # Here it measures the -Property Length by summing it
								       # It gives a table as output ( with count and sum)
	$fileAvg = $fileTotal.Sum / $fileTotal.Count

	"Partisjonen $($item.FullName) befinner seg på $($driveLetter) som er $($driveUsage)% full."
	"Det finnes $($fileCount) filer."
	"Den største er $($fileLargeName)."
	"Som er $($fileLargeSize)B stor."			# This will be in bytes, no matter what
	"Gjennomsnittlig filstørrelse er $($fileAvg)B."
}
else
{
	"Usage: .\fsinfo.ps1 <directory>"
}