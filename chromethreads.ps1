
# Hent alle processene med navn chrome | Kun hent navn, id og tell opp antall tråder | skriv det ut i en tabell uten headers
Get-Process chrome | Select-Object Name, ID, @{Name='ThreadCount';Expression ={$_.Threads.Count}} | Format-Table -HideTableHeaders
