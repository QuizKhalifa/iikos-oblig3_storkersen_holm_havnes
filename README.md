# Obligatorisk oppgave 3 - PowerShell pipelines og scripting

Denne oppgaven best�r av de f�lgende laboppgavene fra kompendiet:

* 11.6.c (Prosesser og tr�der)
* 12.4.c (En prosess sin bruk av virtuelt og fysisk minne)
* 13.10.b (Prosesser og tr�der)
* 13.10.c (Informasjon om deler av filsystemet)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

* Bror-Lauritz Størkersen
* Henrik Havnes
* Ole Martin Holm

## Kvalitetssikring

Vi brukte *"pswh -command 'Invoke-ScriptAnalyzer -EnableExit \*.ps1'"*  for å kvalitetssikre koden. 

pwsh foreslo at vi burde ha fikset på:

| Error | Fix |
| --- | --- |
| Avoid using Write-Host as it may not work on all hosts | Ignoring because "eh" |
| Line has trailing whitespace | Removed trailing whitespace |
| cls is an alias, you shoulnd't use aliases | Using Clear-Host instead |
| Missing BOM encoding for non-ASCII encoded file | Don't care, really |

Etter å ha rettet opp i dette ved å  fikk vi ingen feilmeldinger.


 kan installeres på følgende måte / med følgende kommandoer:

For å utføre eksakt samme kvalitetssikring som vi har gjort, gjør følgende / kjør følgende kommandoer: 
