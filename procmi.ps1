Foreach($argument in $args)
{
    # $argument # Prints the n-th parameter
	If (Get-Process -ErrorAction SilentlyContinue -Id $argument) {
        $filename=".\$argument--$(Get-Date -Uformat %Y%m%d-%H%M%S).meminfo"
        Write-Output "******** Minne info om prosess med PID $argument ********" > $filename
        $vsize=(Get-Process -Id $argument).VirtualMemorySize
        If ($vsize -eq -1) { $vsize=0 }
        else { $vsize=$vsize/1MB }
        Write-Output "Total bruk av virtuelt minne: ${vsize}MB" >> $filename
        $wset=(Get-Process -Id $argument).WorkingSet/1KB
        Write-Output "St�rrelse p� Working Set: ${wset}KB" >> $filename
    }
    else {
        Write-Output "Was not able to find the process with ID ${argument}."
    }
}